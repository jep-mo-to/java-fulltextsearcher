package view;

import controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.ElementModel;

public class Main extends Application{

	public static void main(String[] args)
	{
		Application.launch(Main.class, new String[0]);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/MainFrame.fxml"));
		Controller controller = new Controller();
		controller.init(primaryStage);
		loader.setController(controller);

		AnchorPane root = (AnchorPane)loader.load();
		Scene scene = new Scene(root);
		//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		ElementModel.setStage(primaryStage);
		primaryStage.show();
	}

}
