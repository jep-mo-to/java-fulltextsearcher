package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import model.FileModel;
import model.SearchModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import utilities.FilesReader;

public class Search
{
	private FileModel model;
	static HashMap<String, Integer> data = new HashMap<>();

	public Search()
	{
	}

	public void searchOn(String path) throws NullPointerException
	{
		File directory = new File(path);

		for(File f:directory.listFiles())
		{
			if(f.isDirectory())
			{
				searchOn(f.getPath());
			}else if(isDocument(f))
			{
				System.out.println(f.getPath());
				testFile(f);
			}
		}

		/*private TreeItem<String> getNodesForDirectory(boolean fx, File directory)
		{
			TreeItem<String> root = new TreeItem<String>(directory.getName(), new ImageView(enz));

			if(fx)
				root.setExpanded(true);
			fx = false;

			for(File f : directory.listFiles())
			{
				if(f.isDirectory())
				{
					root.getChildren().add(getNodesForDirectory(fx,f));
				}
				else
				{
					root.getChildren().add(new TreeItem<>(f.getName() , new ImageView(icon)));
				}

			}
			return root;
		}*/
		System.out.println("Bar");
	}
	int x = 0;
	private void testFile(File f)
	{
		//System.out.println(f.getName());
		if(isDocument(f) && !f.isHidden())
		{
			model = new FileModel();
			model.setFile(f);
			model.setPath(f.getPath());
			model.setText(extractText(f));
			int p = extractPoints(model.getText());
			if(p!=0)
			{
				model.setPoints(p);
				data.put(model.getFile().getName(), model.getPoints());
				//System.out.println(model.getFile().getName() +" "+ model.getPoints());
			}
		}

	}

	private Integer extractPoints(String text)
	{
		Integer points = 0;
		String[] words= WhitespaceTokenizer.INSTANCE.tokenize(text);
		//System.out.println(SearchModel.getSearch());
		List<String> search = Arrays.asList(SearchModel.getSearch().split("\\s+"));

		for(int i = 0; i<words.length; i++)
		{
			if(search.contains(words[i]))
			{
				points++;
			}
		}
		return points;
	}

	private String extractText(File f)
	{
		String ex = f.getName().substring(f.getName().lastIndexOf('.')+1);
		switch(ex)
		{
			case "DOC": case "doc":
				return FilesReader.MSWORD97(f);

			case "DOCX": case "docx":
				return FilesReader.MSWORD10(f);

			case "PDF": case "pdf":
				return FilesReader.PDF(f);
		}
		return null;
	}

	private boolean isDocument(File f)
	{
		String ex = f.getName().substring(f.getName().lastIndexOf('.')+1);

		switch(ex)
		{
			case "DOC": case "doc": case "DOCX": case "docx": case "PDF": case "pdf":
				return true;
		}
		return false;
	}

}
