package controller;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.ElementModel;
import model.FileModel;
import model.SearchModel;
import utilities.TableSorter;

public class Controller implements Initializable{

	@FXML TextField textField;
	@FXML TextField txtFieldPath;
	@FXML Button browse_btn;
	@FXML Button search_btn;
	@FXML ListView<String> listView;

	private Stage stage;
	private Search search;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1)
	{
		listView.setOnMouseClicked(new EventHandler<MouseEvent>()
		{
			public void handle(MouseEvent evt)
			{
				if(evt.getClickCount()==2)
				{
					if (Desktop.isDesktopSupported())
						try {
							Desktop.getDesktop().open(new File(SearchModel.getPath() + "\\" + listView.getSelectionModel().getSelectedItem()));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

				}
			}

		});
	}


	public void init(Stage primaryStage)
	{
		this.stage = primaryStage;
	}

	public void onSearch()
	{
		try
		{
			SearchModel.setSearch(textField.getText());
			search = new Search();
			search.searchOn(SearchModel.getPath());
			System.out.println("foo");
		}
		catch(NullPointerException n)
		{}
		finally
		{
			populateList();
		}
	}

	private void populateList()
	{
		listView.getItems().clear();
		listView.setItems(getData());
		for(String q:getData())
			System.out.println(q);
	}

	private ObservableList<String> getData()
	{
		ObservableList<String> content = FXCollections.observableArrayList();
		Search.data = new TableSorter().sortByComparator(Search.data);
		for (Entry<String, Integer> entry : search.data.entrySet())
		{
			content.add(entry.getKey());
			System.out.println(entry.getKey() + " " + entry.getValue());
		}

		return content;
	}
	public void onBrowse()
	{
		try{
			DirectoryChooser dirChooser = new DirectoryChooser();
			String path = dirChooser.showDialog(ElementModel.getStage()).getPath();
			SearchModel.setPath(path);
			/*FileChooser fileChooser = new FileChooser();
			fileChooser.getExtensionFilters().addAll(
					new FileChooser.ExtensionFilter("All Files", "*.*")
					);
			SearchModel.setPath(fileChooser.showSaveDialog(ElementModel.getStage()).getPath());
			System.out.println(SearchModel.getPath());*/
			txtFieldPath.setText(path);
		}catch(Exception e)
		{

		}
	}

}
