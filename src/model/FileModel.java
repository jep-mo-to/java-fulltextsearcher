package model;

import java.io.File;

public class FileModel
{

	private String text;
	private static String path;
	private File file;
	private Integer points;

	public String getText()
	{
		return text;
	}
	public void setText(String text)
	{
		this.text = text;
	}
	public static String getPath()
	{
		return path;
	}
	public void setPath(String path)
	{
		this.path = path;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file)
	{
		this.file = file;
	}
	public Integer getPoints()
	{
		return points;
	}
	public void setPoints(Integer points)
	{
		this.points = points;
	}
	/*public void print()
	{
		//System.out.println(text);
		System.out.println(path);
		System.out.println(points);

		System.out.println("--------------------------");
	}*/

}
