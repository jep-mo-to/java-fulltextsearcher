package model;

import javafx.stage.Stage;

public class ElementModel
{
	private static Stage stage;

	public static void setStage(Stage stage)
	{
		ElementModel.stage = stage;
	}
	public static Stage getStage()
	{
		return stage;
	}
}
