package model;

public class SearchModel
{
	private static String PATH;
	private static String SEARCH;

	public SearchModel()
	{
		PATH = "";
		SEARCH = "";
	}

	public static void setPath(String path)
	{
		PATH = path;
	}

	public static String getPath()
	{
		return PATH;
	}

	public static void setSearch(String search)
	{
		SEARCH = search;
	}

	public static String getSearch()
	{
		return SEARCH;
	}

}
