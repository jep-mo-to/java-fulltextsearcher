package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.openxml4j.exceptions.NotOfficeXmlFileException;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class FilesReader
{
	public static String MSWORD10(File x)
	{
		FileInputStream fs;
		XWPFDocument doc = null;
		try
		{
			fs = new FileInputStream(x.getAbsolutePath());
			doc = new XWPFDocument(fs);
		} catch (IOException e)
		{
			return null;
		}
		catch(OfficeXmlFileException ex)
		{
			return MSWORD97(x);
		}
		catch(NotOfficeXmlFileException exx)
		{ }

		@SuppressWarnings("resource")
		XWPFWordExtractor ex = new XWPFWordExtractor(doc);
		return ex.getText().toLowerCase();
	}

	public static String MSWORD97(File x)
	{
		FileInputStream fs;
		HWPFDocument doc = null;
		try
		{
			fs = new FileInputStream(x.getAbsolutePath());
			doc = new HWPFDocument(fs);
		} catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
		catch(NotOfficeXmlFileException exx)
		{}

		@SuppressWarnings("resource")
		WordExtractor ex = new WordExtractor(doc);
		return ex.getText().toLowerCase();
	}

	public static String PDF(File x)
	{
		PDFTextStripper pdfStripper = null;
		PDDocument document = null;
		try {
			 document = PDDocument.load(x);
			 pdfStripper = new PDFTextStripper();
			 return pdfStripper.getText(document).toLowerCase();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(x.getName());
		}
		catch(NotOfficeXmlFileException exx)
		{ }
		return null;
	}

	public static String Other(File x)
	{
		String txt = "";
		try {
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(new FileReader(x));
			while(scan.hasNext())
				txt += scan.nextLine();

			return txt;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(NotOfficeXmlFileException exx)
		{ return null; }
		return null;
	}

}
