package tuts;

public class Counter
{
	private static long start;

	public static void start()
	{
		start = System.currentTimeMillis();

	}

	public static void end()
	{
		System.out.println(System.currentTimeMillis() - start);
	}

}
